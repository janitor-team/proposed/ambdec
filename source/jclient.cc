// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#include <string.h>
#include "jclient.h"


Jclient::Jclient (const char *jname, const char *jserv) :
    A_thread ("Jclient"),
    _jack_client (0),
    _jack_testip (0),
    _jname (jname),
    _state (ST_BYPASS),
    _n_inp (0),
    _n_out (0),
    _n_act (0),
    _gain (0),
    _mute (0),
    _solo (0),
    _test (0)
{
    init_jack (jname, jserv);   
    _decoder.init (_fsamp);
    memset (_level, 0, MAXOP * sizeof (float));
}


Jclient::~Jclient (void)
{
    if (_jack_client) close_jack ();
}


void Jclient::init_jack (const char *jname, const char *jserv)
{
    int            i;
    char           s [16];
    jack_status_t  stat;
    int            opts;

    opts = JackNoStartServer;
    if (jserv) opts |= JackServerName;
    if ((_jack_client = jack_client_open (jname, (jack_options_t) opts, &stat, jserv)) == 0)
    {
        fprintf (stderr, "Can't connect to JACK\n");
        exit (1);
    }

    jack_set_process_callback (_jack_client, jack_static_process, (void *) this);
    jack_on_shutdown (_jack_client, jack_static_shutdown, (void *) this);

    if (jack_activate (_jack_client))
    {
        fprintf(stderr, "Can't activate JACK.\n");
        exit (1);
    }

    _jname = jack_get_client_name (_jack_client);
    _fsamp = jack_get_sample_rate (_jack_client);
    _fsize = jack_get_buffer_size (_jack_client);
    for (i = 0; i < MAXIP; i++)
    {
	snprintf (s, 16, "in.%d", i);
        _jack_ipports [i] = jack_port_register (_jack_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
	_ipmap [i] = 0;
    }
    _jack_testip = jack_port_register (_jack_client, "test", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
}


void Jclient::close_jack ()
{
    jack_port_unregister (_jack_client, _jack_testip);
    jack_deactivate (_jack_client);
    jack_client_close (_jack_client);
}


void Jclient::jack_static_shutdown (void *arg)
{
    return ((Jclient *) arg)->jack_shutdown ();
}


void Jclient::jack_shutdown (void)
{
    send_event (EV_EXIT, 1);
}


int Jclient::jack_static_process (jack_nframes_t nframes, void *arg)
{
    return ((Jclient *) arg)->jack_process (nframes);
}


int Jclient::jack_process (jack_nframes_t nframes)
{
    unsigned int   i, n, s;
    int            e;
    float          *tip;
    float          *ipp [MAXIP];
    float          *opp [MAXOP];
#ifdef ENABLE_UHJ_INPUT
    float          *uhj_L, *uhj_R;
    float          sr, si, dr, di;
#endif

    s = _state;
    e = get_event_nowait ();
    if (e != EV_TIME) switch (e)
    {
    case EV_GO_BYPASS:  s = ST_BYPASS;  break;
    case EV_GO_SILENCE: s = ST_SILENCE; break;
    case EV_GO_PROCESS: s = ST_PROCESS; break;
    }

    if (_state != ST_BYPASS)
    {
	n = 0;
	memset (opp, 0, MAXOP * sizeof (float *));
        for (i = 0; i < _n_out; i++)
	{
	    opp [i] = (float *) jack_port_get_buffer (_jack_opports [i], nframes);
	}
	if (_state == ST_PROCESS)
	{
  	    for (i = 0; i < _n_inp; i++)
	    {
                ipp [i] = (float *) jack_port_get_buffer (_ipmap [i], nframes);	 
            }
 	    tip = (float *) jack_port_get_buffer (_jack_testip, nframes);
	    _decoder.set_gain (s == ST_PROCESS ? _gain : 0);
	    _decoder.set_mute (_mute);
	    _decoder.set_solo (_solo);
	    _decoder.set_test (_test);
            _decoder.process (nframes, ipp, opp, tip, _level);
	    n = _n_act;
	}
	for (i = n; i < _n_out; i++)
	{
	    memset (opp [i], 0, nframes * sizeof (float));
	}
    }

    if (e != EV_TIME)
    {
        _state = s;
        send_event (e, 1);
    }
    return 0;
}


void Jclient::set_config (AD_conf *C)
{
    _decoder.set_config (C);
    _n_act = C->_dec.nspkr;
}


void Jclient::set_matrix (AD_conf *C)
{
    _decoder.set_matrix (C);
}


void Jclient::set_ipports (AD_conf *C)
{
    int i, m, n;
    
    for (i = n = 0, m = C->_dec.cmask; m; i++, m >>= 1)
    {
        if (m & 1) _ipmap [n++] = _jack_ipports [i];
    }
    _n_inp = n;
    while (n < MAXIP) _ipmap [n++] = 0;
}


void Jclient::create_opports (AD_conf *C)
{
    unsigned int i;
    char s [64];

    delete_opports ();
    _n_out = C->_dec.nspkr;
    for (i = 0; i < _n_out; i++)
    {
	snprintf (s, 64, "out_%s", C->_speakers [i]._label);
        _jack_opports [i] = jack_port_register (_jack_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    }
}


void Jclient::create_opports (int n)
{
    unsigned int i;
    char s [64];

    delete_opports ();
    _n_out = n;
    for (i = 0; i < _n_out; i++)
    {
	snprintf (s, 64, "out_%d", i + 1);
        _jack_opports [i] = jack_port_register (_jack_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    }
}


void Jclient::delete_opports (void)
{
    unsigned int i;

    for (i = 0; i < _n_out; i++)
    {
	jack_port_unregister (_jack_client, _jack_opports [i]);
    }
    _n_out = 0;
}


void Jclient::connect_opports (AD_conf *C)
{
    unsigned int i;
    char s [256];

    disconn_opports ();
    for (i = 0; i < _n_out; i++)
    {
	snprintf (s, 256, "%s:out_%s", _jname, C->_speakers [i]._label);
	jack_connect (_jack_client, s, C->_speakers [i]._jackp);
    }
}


void Jclient::disconn_opports (void)
{
    unsigned int i;

    for (i = 0; i < _n_out; i++)
    {
	jack_port_disconnect (_jack_client, _jack_opports [i]);
    }
}


