// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#ifndef __METER_H
#define	__METER_H


#include <clxclient.h>


class Meter : public X_window
{
public:

    Meter (X_window *parent, int xpos, int ypos);
    ~Meter (void);
    Meter (const Meter&);
    Meter& operator=(const Meter&);

    void update (float v);

    static int load_images (X_display *disp, X_scale_style *scale);

private:

    enum { XS = 4, YS = 163 };

    Pixmap  _pixm;
    int     _k;

    static X_scale_style  *_scale;
    static XImage         *_imag0;
    static XImage         *_imag1;
};


#endif
