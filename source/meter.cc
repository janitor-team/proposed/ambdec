// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#include <math.h>
#include "png2img.h"
#include "meter.h"


X_scale_style *Meter::_scale = 0;
XImage        *Meter::_imag0 = 0;
XImage        *Meter::_imag1 = 0;


Meter::Meter (X_window *parent, int xpos, int ypos) :
    X_window (parent, xpos, ypos, XS, YS, 0),
    _pixm (0),
    _k (-1)
{
    if (!_imag0 || !_imag1 || !_scale) return;
    _pixm = XCreatePixmap (dpy (), win (), XS, YS, disp ()->depth ());
    XPutImage (dpy (), _pixm, dgc (), _imag0, 0, 0, 0, 0, XS, YS); 
    XSetWindowBackgroundPixmap (dpy (), win (), _pixm);
}


Meter::~Meter (void)
{
    if (_pixm) XFreePixmap (dpy (), _pixm);
}


void Meter::update (float v)
{
    int  y, k;

    XSetFunction (dpy (), dgc (), GXcopy);
    k = _scale->calcpix (v);
    if (k > _k)
    {
	y = YS - 1 - k;
        XPutImage (dpy (), _pixm, dgc (), _imag1, 0, y, 0, y, XS, k - _k); 
    }	    
    else if (k < _k)
    {
	y = YS - 1 - _k;
        XPutImage (dpy (), _pixm, dgc (), _imag0, 0, y, 0, y, XS, _k - k); 
    }	    
    _k = k;
    x_clear ();
}


int Meter::load_images (X_display *disp, X_scale_style *scale)
{
    char s [1024];

    _scale = scale;
    snprintf (s, 1024, "%s/meter0.png", SHARED);
    _imag0 = png2img (s, disp, 0);
    snprintf (s, 1024, "%s/meter1.png", SHARED);
    _imag1 = png2img (s, disp, 0);
    return (_imag0 && _imag1) ? 0 : 1;
}
