// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#ifndef __RADBUT_H
#define __RADBUT_H

#include <clxclient.h>


class Radbut : public X_callback
{
public:

    Radbut (X_window *parw, X_callback *xcbh);
    virtual ~Radbut (void);
    
    void init (X_button_style *bst, X_textln_style *tst, const char **text,
               int nbut, int xpos, int ypos, int xsize, int ystep, int cbid);

    void set_stat (int stat);
    int  stat (void) const { return _stat; }

private:

    void handle_callb (int k, X_window *W, _XEvent *E);

    int              _nbut;
    int              _stat;
    int              _cbid;
    X_button       **_butt;  
    X_window        *_parw;
    X_callback      *_xcbh;
};


#endif
