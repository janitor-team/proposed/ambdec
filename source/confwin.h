// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#ifndef __CONFWIN_H
#define	__CONFWIN_H


#include <clxclient.h>
#include "radbut.h"
#include "filewin.h"
#include "adconf.h"


class Confwin : public X_window, public X_callback
{
public:

    Confwin (X_rootwin *parent, X_callback *xcbh, X_resman *xres, int xp, int yp);
    ~Confwin (void);

    void open (AD_conf *C);
    void close (void);
    void handle_time (void);
    AD_conf *conf (void) { return _conf; }

    Confwin (const Confwin&);           // disabled, not to be used
    Confwin& operator=(const Confwin&); // disabled, not to be used

private:
  
    enum { XSIZE = 846, YSIZE = 160 };
    enum { B_LOAD, B_SAVE, B_CANC, B_APPL, B_CON, B_DEC, B_LFD, B_HFD, B_POL, B_VEC };

    void handle_event (XEvent *);
    void handle_callb (int type, X_window *W, XEvent *E);
    void clmesg (XClientMessageEvent *E);
    void newconf (AD_conf *C);
    int  conf2disp (AD_conf *C);
    int  disp2conf (AD_conf *C);

    X_callback     *_xcbh;
    Atom            _atom;
    AD_conf        *_conf;
    Filewin        *_filewin;
    X_window       *_matrwin; 
    Radbut          _r_scale;
    Radbut          _r_ipscl;
    Radbut          _r_delay;
    Radbut          _r_level;
    Radbut          _r_nfeff;
    X_button       *_chbutt [16];
    X_textip       *_focus;
    X_textip       *_t_stat;
    X_textip       *_t_comm;
    X_textln       *_l_xfreq;
    X_textip       *_t_xfreq;
    X_textln       *_l_ratio;
    X_textip       *_t_ratio;
    X_tbutton      *_b_load;
    X_tbutton      *_b_save;
    X_tbutton      *_b_canc;
    X_tbutton      *_b_appl;
};


#endif
