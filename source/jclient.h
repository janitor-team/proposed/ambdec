// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#ifndef __JCLIENT_H
#define __JCLIENT_H


#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <clthreads.h>
#include <jack/jack.h>
#include "decoder.h"
#include "global.h"
#include "adconf.h"


class Jclient : public A_thread
{
public:

    Jclient (const char *jname, const char *jserv);
    ~Jclient (void);

    const char *jname (void) const { return _jname; }
    const float *get_level (void) const { return _level; }

    Decoder _decoder;

    void set_config (AD_conf *C);
    void set_matrix (AD_conf *C);
    void set_ipports (AD_conf *C); 
    void delete_ipports (void); 
    void create_opports (AD_conf *C); 
    void create_opports (int n); 
    void delete_opports (void); 
    void connect_opports (AD_conf *C); 
    void disconn_opports (void); 
    void set_gain (float v) { _gain = v; } 
    void set_mute (uint64_t b) { _mute |= b; }
    void clr_mute (uint64_t b) { _mute &= ~b; }
    void set_solo (uint64_t b) { _solo |= b; }
    void clr_solo (uint64_t b) { _solo &= ~b; }
    void set_test (int t) { _test = t; }

private:

    enum { ST_BYPASS, ST_SILENCE, ST_PROCESS };

    void  init_jack (const char *jname, const char *jserv);
    void  close_jack (void);
    void  jack_shutdown (void);
    int   jack_process (jack_nframes_t nframes);

    virtual void thr_main (void) {}

    jack_client_t  *_jack_client;
    jack_port_t    *_jack_testip;
    jack_port_t    *_jack_ipports [MAXIP];
    jack_port_t    *_jack_opports [MAXOP];
    const char     *_jname;
    unsigned int    _state;
    unsigned int    _fsamp;
    unsigned int    _fsize;
    jack_port_t    *_ipmap [MAXIP];
    unsigned int    _n_inp;
    unsigned int    _n_out;
    unsigned int    _n_act;
    float           _gain;
    uint64_t        _mute;
    uint64_t        _solo;
    int             _test;
    float           _level [MAXOP];

    static void jack_static_shutdown (void *arg);
    static int  jack_static_process (jack_nframes_t nframes, void *arg);
};


#endif
