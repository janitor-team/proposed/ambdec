// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <clthreads.h>
#include <sys/mman.h>
#include <signal.h>
#include "jclient.h"


static Jclient   *jclient = 0;
static ITC_ctrl   itcctrl;
static bool       stop = false;


static void help (void)
{
    fprintf (stderr, "\nAmbDec-%s (command line version)\n\n", VERSION);
    fprintf (stderr, "  (C) 2006-2011 Fons Adriaensen  <fons@linuxaudio.org>\n\n");
    fprintf (stderr, "Usage: ambdec_cli config-file\n");
    exit (1);
}


static void sigint_handler (int)
{
    signal (SIGINT, SIG_IGN);
    stop = true;
}


static void syncdec (int ev)
{
    itcctrl.send_event (ev, 1);
    itcctrl.get_event (1 << ev);
}


static void applconf (AD_conf *C)
{
    syncdec (EV_GO_SILENCE);
    if (C->_state & AD_conf::ST_OUTS)
    {
	jclient->disconn_opports ();
        syncdec (EV_GO_BYPASS);
	jclient->create_opports (C);
        syncdec (EV_GO_SILENCE);
        jclient->connect_opports (C);
    }
    if (C->_state & AD_conf::ST_INPS) jclient->set_ipports (C);
    if (C->_state & AD_conf::ST_OPTS) jclient->set_config (C);
    if (C->_state & AD_conf::ST_MATR) jclient->set_matrix (C);
    syncdec (EV_GO_PROCESS);
    C->_state = 0;
}


int main (int ac, char *av [])
{
    int      ev;
    AD_conf  conf;

    if (ac < 2) help ();

    if (conf.load (av [1]))
    {
        fprintf (stderr, "Can't load configuration '%s'\n", av [1]);
	return 2;
    }

    jclient = new Jclient ("Ambdec", 0);

    ITC_ctrl::connect (&itcctrl, EV_GO_BYPASS,  jclient,  EV_GO_BYPASS);
    ITC_ctrl::connect (&itcctrl, EV_GO_SILENCE, jclient,  EV_GO_SILENCE);
    ITC_ctrl::connect (&itcctrl, EV_GO_PROCESS, jclient,  EV_GO_PROCESS);
    ITC_ctrl::connect (jclient,  EV_GO_BYPASS,  &itcctrl, EV_GO_BYPASS);
    ITC_ctrl::connect (jclient,  EV_GO_SILENCE, &itcctrl, EV_GO_SILENCE);
    ITC_ctrl::connect (jclient,  EV_GO_PROCESS, &itcctrl, EV_GO_PROCESS);
    ITC_ctrl::connect (jclient,  EV_EXIT,       &itcctrl, EV_EXIT);

    if (mlockall (MCL_CURRENT | MCL_FUTURE)) fprintf (stderr, "Warning: memory lock failed.\n");
    signal (SIGINT, sigint_handler); 

    conf._state = AD_conf::ST_ALL;
    applconf (&conf);
    jclient->set_gain (1.0f);

    itcctrl.set_time (0);
    while (! stop)
    {
	ev = itcctrl.get_event_timed ();
	switch (ev)
        {
	case Esync::EV_TIME:
	    itcctrl.inc_time (100000);
	    break;

	case EV_EXIT:
	    printf ("Kicked out by jackd !\n");
	    stop = true;
	    break;
	}
    }

    delete jclient;
   
    return 0;
}



